package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.model.User;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UserController {
	
	@Autowired
	UserDAO userDao;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@GetMapping("getUsers")
	public List<User> getUsers() {
		List<User> userList = userDao.getUsers();
		return userList;
	}
	
	@PostMapping("registerUser")
	public User addUser(@RequestBody User user){
		return userDao.addUser(user);	
	}

	@GetMapping("getLogin/{emailId}/{password}")
	public User getLogin(@PathVariable("emailId") String emailId, @PathVariable("password") String password){
		return userDao.getLogin(emailId,password);
	}
	
}
