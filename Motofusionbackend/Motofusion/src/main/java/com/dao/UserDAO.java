package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.User;

@Service
public class UserDAO {
	
	@Autowired
	UserRepository userRepo;

	public User addUser(User user) {
		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
		String encryptedPassword = bcrypt.encode(user.getPassword());
		user.setPassword(encryptedPassword);
		return userRepo.save(user);
	}


	public List<User> getUsers() {
		return userRepo.findAll();
	}


	public User getLogin(String emailId, String password) {
		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
		String encryptedPassword = bcrypt.encode(password);
		return userRepo.getLogin(emailId,password);
//		User user=userRepo.findByName(emailId);
//		if(emailId!=null&& BCrypt.checkpw(password, user.getPassword())){
//			return user;
//		}
//		
//		else{
//			return null;
//		}
	}
	}

	

	
	
	


