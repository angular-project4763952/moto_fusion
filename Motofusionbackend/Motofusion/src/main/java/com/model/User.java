package com.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class User {
	
	@Id
	@GeneratedValue()
	private int userId;
	private String userName;
	private String gender;
	private Date dob;
	private Long phoneNumber;
	private String emailId;
	private String password;
	
	
	public User() {
		super();
	}
		
	public User(int userId,String userName, String gender, Date dob,long phoneNumber,String emailId, String password) {
		super();
		this.userId   = userId;
		this.userName = userName;
		this.gender   = gender;
		this.dob      = dob;
		this.phoneNumber = phoneNumber;
		this.emailId  = emailId;
		this.password = password;
		
	}


	public long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public Date getDob() {
		return dob;
	}


	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getEmailId() {
		return emailId;
	}


	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", gender=" + gender + ", dob=" + dob
				+ ", phoneNumber=" + phoneNumber + ", emailId=" + emailId + ", password=" + password + "]";
	}

}
