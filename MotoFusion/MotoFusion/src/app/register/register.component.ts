import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { NgToastService } from 'ng-angular-popup';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { response } from 'express';
import { getLocaleExtraDayPeriodRules } from '@angular/common';
import { Router } from '@angular/router';
import { error } from 'node:console';
// import { CustomerServiceService} from '../customer-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {
  user: any;

  registerForm = this.fb.group({
    userName: ['', Validators.required],
    emailId: ['', [Validators.required, Validators.email]],
    password: [
      '',
      [
        Validators.required,
        Validators.minLength(6),
        Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/)
      ]
    ],
    phoneNumber: ['', Validators.required],
    gender: ['', Validators.required],
    dob: ['', Validators.required]

  })



  constructor(private service: UserService, private fb: FormBuilder, private router: Router,private toast : NgToastService) {
    this.user = {
      userId: '',
      userName: '',
      gender: '',
      dob: '',
      phoneNumber: '',
      emailId: '',
      password: ''
    }


  }


  registerSubmit(registerForm: any) {
    console.log(registerForm);

    if (
      !registerForm.userId ||
      !registerForm.userName ||
      !registerForm.gender ||
      !registerForm.dob ||
      !registerForm.phoneNumber ||
      !registerForm.emailId ||
      !registerForm.password

    // !this.registerForm.get('userId').value ||   
    // !this.registerForm.get('userName').value ||
    // !this.registerForm.get('gender').value ||
    // !this.registerForm.get('dob').value ||
    // !this.registerForm.get('phoneNumber').value ||
    // !this.registerForm.get('emailId').value ||
    // !this.registerForm.get('password').value
    ) {
      this.toast.warning({ detail: "Error Message", summary: "All fields must be Enter", duration: 5000, sticky: true, position: 'topRight' })
    
    }



    this.user.userId = registerForm.userId.value;
    this.user.userName = registerForm.userName.value;
    this.user.gender = registerForm.gender.value;
    this.user.dob = registerForm.dob.value;
    this.user.phoneNumber = registerForm.phoneNumber.value;
    this.user.emailId = registerForm.emailId.value;
    this.user.password = registerForm.password.value;

  // this.user.userId = this.registerForm.get('userId').value;
  // this.user.userName = this.registerForm.get('userName').value;
  // this.user.gender = this.registerForm.get('gender').value;
  // this.user.dob = this.registerForm.get('dob').value;
  // this.user.phoneNumber = this.registerForm.get('phoneNumber').value;
  // this.user.emailId = this.registerForm.get('emailId').value;
  // this.user.password = this.registerForm.get('password').value;

    console.log(this.user);

    this.service.registerUser(this.user).subscribe((data: any) => {
      console.log(data);
      this.toast.success({ detail: "Registration Successful!", summary: "Redirecting to login page.", duration: 5000, sticky: true, position: 'topRight'});
      this.router.navigate(['login']);
      (error: any) => {
        console.error(error);
        this.toast.error({detail: "Registration failed", summary: "Please try again.", duration: 5000, sticky: true, position: 'topRight'});
      }

    });
  }

  ngOnInit() {
  }
}
