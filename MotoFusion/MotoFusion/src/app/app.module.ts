import { NgModule } from '@angular/core';
import { BrowserModule, provideClientHydration } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { IonicModule } from '@ionic/angular';
import { RegisterComponent } from './register/register.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgToastModule } from 'ng-angular-popup';
import { NgxCaptchaModule } from 'ngx-captcha';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CarouselComponent } from './carousel/carousel.component';
import { CarouselModule }  from 'ngx-bootstrap/carousel';
import { CarComponent } from './car/car.component';
import { CardModule } from 'primeng/card';
import { CartComponent } from './cart/cart.component';
import { ButtonModule } from 'primeng/button';
import { BikeComponent } from './bike/bike.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    NavbarComponent,
    HomeComponent,
    CarouselComponent,
    CarComponent,
    CartComponent,
    BikeComponent
  ],
  imports: [
    IonicModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgToastModule,
    HttpClientModule,
    NgxCaptchaModule,
    RouterModule,
    ReactiveFormsModule,
    CarouselModule,
    CardModule,
    ButtonModule
    
  ],
  providers: [
    provideClientHydration()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
