import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  loginStatus : any;
  

  constructor(private http: HttpClient) {
    this.loginStatus = false;
   }



   setLoginStatus(){
    this.loginStatus = true;
    
   }

   registerUser(user : any):any{
    return this.http.post('http://localhost:8098/registerUser', user);
   }

   getLogin(emailId: any, password: any): any{
      return this.http.get('http://localhost:8098/getLogin/' + emailId + '/' + password).toPromise(); 
   }


  
  }
