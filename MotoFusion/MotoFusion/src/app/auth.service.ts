import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  cartItems: any;

  constructor(private http: HttpClient) {

    this.cartItems = [];

   }

   addToCart(car: any) {
    this.cartItems.push(car);
  }

  addToCart1(bike: any) {
    this.cartItems.push(bike);
  }

  getCartItems(): any {
    return this.cartItems;
  }

  setCartItems() {
    this.cartItems.splice();
  }
}
