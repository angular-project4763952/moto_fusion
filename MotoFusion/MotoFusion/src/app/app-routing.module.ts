import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { CartComponent } from './cart/cart.component';
import { CarComponent } from './car/car.component';
import { BikeComponent } from './bike/bike.component';

const routes: Routes = [
  {path: '',           redirectTo: '/home', pathMatch: 'full'},
  {path: 'home',           component:HomeComponent},
  {path:'login',       component:LoginComponent},
  {path:'register',    component:RegisterComponent},
  {path:'cart',        component:CartComponent},
  {path:'car',         component:CarComponent},
  {path: 'bike',       component:BikeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
