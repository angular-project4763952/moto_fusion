import { Component } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrl: './carousel.component.css'
})
export class CarouselComponent {
  images = [
    { path: 'assets/images/caroselbike1.jpg' },
    { path: 'assets/images/caroselcar1.jpg' },
  ];
}
