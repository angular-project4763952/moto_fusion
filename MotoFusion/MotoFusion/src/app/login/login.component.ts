import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { NgToastService } from 'ng-angular-popup';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit {
  


  emailId : any;
  password: any;
  siteKey : string;

  user :any;
  

  constructor(private router: Router, private service: UserService, private toast:NgToastService){
    this.siteKey = '6LcDmkMpAAAAAMcVV4d1k-EuSVzScszelrt1JnGd';
  }
  ngOnInit() {
   
  }

   async loginSubmit(loginForm: any) {
    this.user = null;
    localStorage.setItem("emailId", loginForm.emailId);


    if(loginForm.emailId === "HR" && loginForm.password === "HR") {
      this.service.setLoginStatus();
     this.toast.success({ detail: "Success Message", summary: "Login in successful", duration: 5000, sticky: true, position: 'topRight' })
     this.router.navigate(['/home']);
    } else {
      await  this.service.getLogin(loginForm.emailId, loginForm.password).then((data : any)=>{
        console.log(data);
        this.user = data;
      });

      if(this.user != null){
        this.service.setLoginStatus();
        this.router.navigate(['/home']);
      }
      this.toast.error({ detail: "Error Message", summary: "Invalid Credentials", duration: 5000, sticky: true, position: 'topRight' })
    }


  }

  submit() {
    alert("EmailId: " + this.emailId + "\nPassword: " + this.password);
    console.log(this.emailId);
    console.log(this.password);
  }

}
