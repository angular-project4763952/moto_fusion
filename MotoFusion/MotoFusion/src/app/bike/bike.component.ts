import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-bike',
  templateUrl: './bike.component.html',
  styleUrl: './bike.component.css'
})
export class BikeComponent  implements OnInit{
  bikes: any;
  cartBikes: any


  constructor(private service: AuthService) {
    this.cartBikes = [];

    this.bikes = [
      {
        name: 'BAJAJ CHETAK',
        description: 'Description for Product 1',
        price: 19,
        rating: 4.5,
        image: 'assets/images/Bike Images/Bajaj/chetak.jpg'
      },
      {
        name: 'BAJAJ PULSAR-125',
        description: 'Description for Product 1',
        price: 19,
        rating: 4.5,
        image: 'assets/images/Bike Images/Bajaj/pulsar-125.jpeg'
      },
      {
        name: 'BAJAJ PULSAR-150',
        description: 'Description for Product 1',
        price: 19,
        rating: 4.5,
        image: 'assets/images/Bike Images/Bajaj/pulsar-150.jpeg'
      },
      {
        name: 'BAJAJ PULSAR-NS200',
        description: 'Description for Product 1',
        price: 19,
        rating: 4.5,
        image: 'assets/images/Bike Images/Bajaj/Pulsar-NS200.jpg'
      },
      {
        name: 'TVS APACHE RR 310',
        description: 'Description for Product 1',
        price: 19,
        rating: 4.5,
        image: 'assets/images/Bike Images/TVS/Apache RR 310.jpeg'
      },
      {
        name: 'TVS APACHE RTR 160 4V',
        description: 'Description for Product 1',
        price: 19,
        rating: 4.5,
        image: 'assets/images/Bike Images/TVS/Apache RTR 160 4V.jpeg'
      },
      {
        name: 'TVS JUPITER-ZX',
        description: 'Description for Product 1',
        price: 19,
        rating: 4.5,
        image: 'assets/images/Bike Images/TVS/Jupiter-ZX.jpg'
      },
      {
        name: 'TVS RAIDER',
        description: 'Description for Product 1',
        price: 19,
        rating: 4.5,
        image: 'assets/images/Bike Images/TVS/Raider.jpeg'
      },
      {
        name: 'TVS SPORT',
        description: 'Description for Product 1',
        price: 19,
        rating: 4.5,
        image: 'assets/images/Bike Images/TVS/Sport.jpeg'
      },
      // {
      //   name: 'KIA SELTOS',
      //   description: 'Description for Product 1',
      //   price: 19,
      //   rating: 4.5,
      //   image: 'assets/images/car images/Kia/Seltos.jpeg'
      // },
      // {
      //   name: 'KIA SONET',
      //   description: 'Description for Product 1',
      //   price: 19,
      //   rating: 4.5,
      //   image: 'assets/images/car images/Kia/Sonet.jpeg'
      // },
      // {
      //   name: 'MAHINDRA BOLERO NEO',
      //   description: 'Description for Product 1',
      //   price: 19,
      //   rating: 4.5,
      //   image: 'assets/images/car images/Mahindra/boleroneo.jpeg'
      // },
      // {
      //   name: 'MAHINDRA SCORPIO',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Mahindra/scorpio.jpeg'
      // },
      // {
      //   name: 'MAHINDRA SCORPION',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Mahindra/scorpion.jpeg'
      // },
      // {
      //   name: 'MAHINDRA THAR',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Mahindra/thar.jpeg'
      // },
      // {
      //   name: 'MAHINDRA XUV300',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Mahindra/xuv300.jpeg'
      // },
      // {
      //   name: 'MAHINDRA XUV400',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Mahindra/xuv400.jpeg'
      // },
      // {
      //   name: 'MAHINDRA XUV700',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Mahindra/xuv700.jpeg'
      // },
      // {
      //   name: 'MARUTI SUZUKI BALENO',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Maruti Suzuki/Baleno.jpeg'
      // },
      // {
      //   name: 'MARUTI SUZUKI BREZZA',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Maruti Suzuki/Brezza.jpeg'
      // },
      // {
      //   name: 'MARUTI SUZUKI ERTIGA',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Maruti Suzuki/Ertiga.jpeg'
      // },
      // {
      //   name: 'MARUTI SUZUKI GRAND VITARA',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Maruti Suzuki/Grand Vitara.jpeg'
      // },
      // {
      //   name: 'MARUTI SUZUKI JIMNY',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Maruti Suzuki/Jimny.jpeg'
      // },
      // {
      //   name: 'MARUTI SUZUKI SWIFT',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Maruti Suzuki/Swift.jpeg'
      // },
      // {
      //   name: 'MARUTI SUZUKI SWIFT DZIRE',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Maruti Suzuki/Swiftdzire.jpeg'
      // },
      //   {
      //   name: 'MARUTI SUZUKI XL6',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Maruti Suzuki/xl6.jpeg'
      // },
      // {
      //   name: 'TATA ALTROZ',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Tata/Altroz.jpeg'
      // },
      // {
      //   name: 'TATA HARRIER',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Tata/Harrier.jpeg'
      // },
      // {
      //   name: 'TATA NEXON',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Tata/Nexon.jpeg'
      // },
      // {
      //   name: 'TATA PUNCH',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Tata/Punch.jpeg'
      // },
      // {
      //   name: 'TATA SAFARI',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Tata/Safari.jpeg'
      // },
      // {
      //   name: 'TATA TIAGO',
      //   description: 'Description for Product 1',
      //   price: '$19.99',
      //   rating: 4.5,
      //   image: 'assets/images/car images/Tata/Tiago.jpeg'
      // }
      
    ];


  }

  ngOnInit() {
  }

  addToCart1(bike: any) {
    // this.cartProducts.push(product);
    // localStorage.setItem("cartItems", JSON.stringify(this.cartProducts));

    //Cart using Service
    this.service.addToCart(bike);
  }

}
