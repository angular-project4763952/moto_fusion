import { Component } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent {
  localStorageData: any;
  emailId: any;
  total: number;
  cars: any;

  constructor(private service: AuthService) {
    this.total = 0;
    // this.emailId = localStorage.getItem('emailId');

    this.cars = service.getCartItems();

    this.cars.forEach((element: any) => {
      this.total = this.total +parseInt(element.price);
    });

    // this.localStorageData = localStorage.getItem('cartItems');
    // this.products = JSON.parse(this.localStorageData);
    // console.log(this.products);
  }

  deleteCartCar(car: any) {
    const i = this.cars.findIndex((element: any) => {
      return element.id == car.id;
    });
    this.cars.splice(i, 1);
    this.total = this.total - car.price;
  }

  purchase() {
    this.cars = [];
    this.total = 0;
    this.service.setCartItems();
  }

}
